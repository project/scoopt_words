﻿$Id$

Drupal scoopt_words module README
---------------------------------
Copyright (C) 2006  Ramiro Gómez (http://www.ramiro.org)

This program is free software; you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

(see LICENSE.txt)

Requirements
------------
This module requires Drupal 4.7.

This module does not yet offer PostgreSQL support. If you would like to contribu
te to this module by creating the appropriate PostgreSQL schema, please submit your code at http://drupal.org/project/issues/scoopt_words

Overview:
--------
The scoopt_words module enables admins and user's to add a banner link 
(badge) to the ScooptWords service to nodes:

The site owner can decide:
- Which badge to use (currently small and larger).
- What node types to display badges in.
- Whether user's can add their own Scoopt ID to earn revenue.
- Whether the badge is displayed in teaser view, full page view
  or both.
- Whether the badge should be added after the body text or in a block.
- Decide what roles get to see/use the badge and what roles may use their individual Scoopt ID.

Installation and configuration:
------------------------------
Installation is as simple as creating a directory named 'scoopt_words' 
in your 'modules' directory and copying the module's files into it, 
then enable the module in 'administer >> modules'.

If the database table is not created after enabling the module install 
it manually from the command line:
mysql -p -u user my_drupal_db < scoop_words.mysql

For configuration options go to 
'administer >> settings >> scoopt_words'.

For permission settings go to 'administer >> access control.
